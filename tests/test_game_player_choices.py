"""
Test suite to test all weapon choice outcomes.
"""

import unittest

from src.game import Game
from src.utils.enums import Weapons
from src.utils.exceptions import WeaponsError


class TestGameWeaponChoices(unittest.TestCase):

    def test_player_selection_integer_invalid(self):
        player_choice = 67
        with self.assertRaises(WeaponsError) as error:
            Game.get_player_choice(player_choice)
        self.assertEqual(error.exception.message, 'Selected weapon is invalid!')

    def test_player_selection_integer_valid(self):
        player_choice = 3
        self.assertIs(Game.get_player_choice(player_choice), Weapons(player_choice))

    def test_player_selection_string_invalid(self):
        player_choice = 'Kirk'
        with self.assertRaises(WeaponsError) as error:
            Game.get_player_choice(player_choice)
        self.assertEqual(error.exception.message, 'Selected weapon is invalid!')

    def test_player_selection_string_valid(self):
        player_choice = 'spock'
        self.assertIs(Game.get_player_choice(player_choice), Weapons[player_choice.upper()])
