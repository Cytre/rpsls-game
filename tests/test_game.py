"""
Test suite to test all weapon choice outcomes.
"""

import unittest

from src.game import Game
from src.utils.exceptions import WeaponsError


class TestGame(unittest.TestCase):

    def test_get_info_file_error(self):
        file_name = 'incorrect_file'
        self.assertEqual(Game.get_info(file_name), f'Error opening file: {file_name}')

    def test_get_info_file_help(self):
        file_name = 'help'
        self.assertIs(type(Game.get_info(file_name)), str)
        self.assertTrue(len(Game.get_info(file_name)) > len(f'Error opening file: {file_name}'))

    def test_get_info_file_rules(self):
        file_name = 'rules'
        self.assertIs(type(Game.get_info(file_name)), str)
        self.assertTrue(len(Game.get_info(file_name)) > len(f'Error opening file: {file_name}'))

    def test_play_game_success(self):
        game = Game()
        player_choice = 'spock'
        self.assertIs(type(game.play(player_choice)), str)
        self.assertTrue(len(game.play(player_choice)) > 50)

    def test_play_game_fail(self):
        game = Game()
        player_choice = 'kirk'
        with self.assertRaises(WeaponsError) as error:
            game.play(player_choice)
        self.assertEqual(error.exception.__str__(), 'kirk -> Selected weapon is invalid!')
