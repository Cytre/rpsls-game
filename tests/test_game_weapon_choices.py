"""
Test suite to test all weapon choice outcomes.
"""

import unittest

from src.game import Game
from src.utils.enums import Weapons


class TestGameWeaponChoices(unittest.TestCase):
    COMPUTER_WINS = 'Computer wins!'
    PLAYER_WINS = 'Player wins!'
    PLAYERS_TIE = 'Player and computer tie!'

    def test_rock_beats_lizard_player(self):
        player_choice = Weapons.ROCK.value
        computer_choice = Weapons.LIZARD.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_rock_beats_scissors_player(self):
        player_choice = Weapons.ROCK.value
        computer_choice = Weapons.SCISSORS.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_rock_beats_lizard_computer(self):
        player_choice = Weapons.LIZARD.value
        computer_choice = Weapons.ROCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_rock_beats_scissors_computer(self):
        player_choice = Weapons.SCISSORS.value
        computer_choice = Weapons.ROCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_rock_ties_rock(self):
        player_choice = Weapons.ROCK.value
        computer_choice = Weapons.ROCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYERS_TIE)

    def test_paper_beats_rock_player(self):
        player_choice = Weapons.PAPER.value
        computer_choice = Weapons.ROCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_paper_beats_spock_player(self):
        player_choice = Weapons.PAPER.value
        computer_choice = Weapons.SPOCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_paper_beats_rock_computer(self):
        player_choice = Weapons.ROCK.value
        computer_choice = Weapons.PAPER.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_paper_beats_spock_computer(self):
        player_choice = Weapons.SPOCK.value
        computer_choice = Weapons.PAPER.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_paper_ties_paper(self):
        player_choice = Weapons.PAPER.value
        computer_choice = Weapons.PAPER.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYERS_TIE)

    def test_scissors_beats_paper_player(self):
        player_choice = Weapons.SCISSORS.value
        computer_choice = Weapons.PAPER.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_scissors_beats_lizard_player(self):
        player_choice = Weapons.SCISSORS.value
        computer_choice = Weapons.LIZARD.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_scissors_beats_paper_computer(self):
        player_choice = Weapons.PAPER.value
        computer_choice = Weapons.SCISSORS.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_scissors_beats_lizard_computer(self):
        player_choice = Weapons.LIZARD.value
        computer_choice = Weapons.SCISSORS.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_scissors_ties_scissors(self):
        player_choice = Weapons.SCISSORS.value
        computer_choice = Weapons.SCISSORS.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYERS_TIE)

    def test_lizard_beats_spock_player(self):
        player_choice = Weapons.LIZARD.value
        computer_choice = Weapons.SPOCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_lizard_beats_paper_player(self):
        player_choice = Weapons.LIZARD.value
        computer_choice = Weapons.PAPER.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_lizard_beats_spock_computer(self):
        player_choice = Weapons.SPOCK.value
        computer_choice = Weapons.LIZARD.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_lizard_beats_paper_computer(self):
        player_choice = Weapons.PAPER.value
        computer_choice = Weapons.LIZARD.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_lizard_ties_lizard(self):
        player_choice = Weapons.LIZARD.value
        computer_choice = Weapons.LIZARD.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYERS_TIE)

    def test_spock_beats_scissors_player(self):
        player_choice = Weapons.SPOCK.value
        computer_choice = Weapons.SCISSORS.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_spock_beats_rock_player(self):
        player_choice = Weapons.SPOCK.value
        computer_choice = Weapons.ROCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYER_WINS)

    def test_spock_beats_scissors_computer(self):
        player_choice = Weapons.SCISSORS.value
        computer_choice = Weapons.SPOCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_spock_beats_rock_computer(self):
        player_choice = Weapons.ROCK.value
        computer_choice = Weapons.SPOCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.COMPUTER_WINS)

    def test_spock_ties_spock(self):
        player_choice = Weapons.SPOCK.value
        computer_choice = Weapons.SPOCK.value
        self.assertEqual(Game.calculate_winner(player_choice, computer_choice), self.PLAYERS_TIE)
