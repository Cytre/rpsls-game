# Rock Paper Scissors Lizard Spock
Original code created in 2015, but this version has improvements added.

## Copyright
Copyright 2021, Kevin D. Reeves. All rights reserved.

All code written by me falls under the U.S. copyright law.

I do not give permission to:

- redistribute the code, copies of the code, or any modified versions of the code for any purpose other than my own.
- distribute the code, copies of the code, or any modified versions of the code to others or any purpose other than my own.
- use all or parts of the code for a commercial project.

I do give permission to:

- run this code for any purpose other than my own.
- study the code and how this program works, and change or modify for learning purposes only.
