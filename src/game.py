"""
Rock Paper Scissors Lizard Spock main game class.

Author:
    Kevin D. Reeves <kevin@kevindreeves.com>
Copyright:
    2015 Kevin D. Reeves
Module:
    src.game
Package:
    src
Since:
    2015-06-01
"""

import os
import random

from src.utils.enums import Weapons
from src.utils.exceptions import WeaponsError


class Game:
    """Main class for the game."""

    @staticmethod
    def calculate_winner(player_choice: int, computer_choice: int) -> str:
        """
        Compute winner between player and computer choices.

        Parameters
        ----------
        player_choice: int
            The player's weapon choice.
        computer_choice: int
            The computer's weapon choice.

        Returns
        -------
        str
        """
        difference = (computer_choice - player_choice) % 5

        if difference in (1, 2):
            return 'Computer wins!'
        if difference in (3, 4):
            return 'Player wins!'

        return 'Player and computer tie!'

    @staticmethod
    def get_info(file: str) -> str:
        """
        Parameters
        ----------
        file : str
            File name to be loaded.

        Returns
        -------
        str
        """
        current_path = os.path.dirname(__file__)
        info_file = os.path.join(current_path, os.pardir, 'bin', 'text', f'{file}.txt')

        try:
            with open(info_file, 'r', encoding='utf-8') as text:
                info_text = text.read()
                text.close()
        except IOError:
            info_text = f'Error opening file: {file}'

        return info_text

    @staticmethod
    def get_player_choice(choice) -> Weapons:
        """
        Returns the player selected weapon.

        Parameters
        ----------
        choice : str|int
            Player's choice as a string or integer.

        Returns
        -------
        Weapons
            The player's selected weapon.

        Raises
        ------
        WeaponsError
            Raises WeaponsError when the player choice cannot be found in the Weapons enum.
        """
        try:
            if isinstance(choice, str):
                return Weapons[choice.upper()]
            return Weapons(choice)
        except(KeyError, ValueError) as error:
            raise WeaponsError(choice) from error

    def play(self, choice) -> str:
        """
        Plays a round of the game.

        Parameters
        ----------
        choice : str|int
            Player's choice as a string or integer.

        Returns
        -------
        str

        Raises
        ------
        WeaponsError :
            Raises a WeaponsError via get_player_choice method.
        """
        player_choice = self.get_player_choice(choice)
        computer_choice = random.choice(list(Weapons))
        winner = self.calculate_winner(player_choice.value, computer_choice.value)

        player_text = f'Player chooses {Weapons(player_choice).name.title()}.'
        computer_text = f'Computer chooses {Weapons(computer_choice).name.title()}.'

        return f'{player_text}\n{computer_text}\n{winner}\n'
