"""
Collection of enums used in the program.

Author:
    Kevin D. Reeves <kevin@kevindreeves.com>
Copyright:
    2021 Kevin D. Reeves
Module:
    src.utils.enums
Package:
    src.utils
"""

from enum import Enum


class Weapons(Enum):
    """Weapons Enum"""
    ROCK = 0
    PAPER = 2
    SCISSORS = 4
    LIZARD = 3
    SPOCK = 1
