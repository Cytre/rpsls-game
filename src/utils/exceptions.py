"""
Collection of exceptions used in the program.

Author:
    Kevin D. Reeves <kevin@kevindreeves.com>
Copyright:
    2021 Kevin D. Reeves
Module:
    src.utils.exceptions
Package:
    src.utils
"""


class WeaponsError(Exception):
    """Raised when weapon selection is invalid"""

    def __init__(self, weapon, message='Selected weapon is invalid!'):
        self.weapon = weapon
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'{self.weapon} -> {self.message}'
