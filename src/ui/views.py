"""
Library containing all view related classes.

Author:
    Kevin D. Reeves <kevin@kevindreeves.com>
Copyright:
    2021 Kevin D. Reeves
Module:
    src.ui.views
Package:
    src.ui
"""

import tkinter as tk

from abc import ABC, abstractmethod
from tkinter.messagebox import showinfo
from tkinter.scrolledtext import ScrolledText
from src.utils.enums import Weapons


class AbstractView(ABC):
    """View base class abstraction."""

    @abstractmethod
    def info(self, text: str) -> None:
        """
        Displays an info prompt containing the text.

        Parameters
        ----------
        text : str
            The info text to be displayed in the info prompt.
        """

    @abstractmethod
    def play(self, text: str) -> None:
        """
        Plays a round of the game and displays result.

        Parameters
        ----------
        text : str
            Result of the game round.
        """

    @abstractmethod
    def setup(self, controller) -> None:
        """
        Set up the interface for the game.

        Parameters
        ----------
        controller :
            Controller instance.
        """

    @abstractmethod
    def run(self) -> None:
        """Runs the view's main loop."""


class TkView(AbstractView):
    """TkInter view class"""

    text_box: ScrolledText

    def __init__(self) -> None:
        self.window = tk.Tk()
        self.window.geometry('400x150')
        self.window.resizable(False, False)
        self.window.title('Rock Paper Scissors Lizard Spock')

    def create_menu(self, controller) -> None:
        """
        Creates and adds the menu bar to the root window.

        Parameters
        ----------
        controller :
            Controller instance.
        """
        menu_bar = tk.Menu(self.window)
        self.window.config(menu=menu_bar)

        file_menu = tk.Menu(menu_bar)
        file_menu.add_command(label='Exit', command=self.window.destroy)

        info_menu = tk.Menu(menu_bar)
        info_menu.add_command(label='Help', command=controller.help)
        info_menu.add_command(label='Rules', command=controller.rules)

        menu_bar.add_cascade(label='File', menu=file_menu)
        menu_bar.add_cascade(label='Help', menu=info_menu)

    def create_text_area(self) -> None:
        """Creates and adds the scrolled text area to the root."""
        frame = tk.Frame(self.window)
        frame.pack(fill=tk.BOTH, expand=True)

        self.text_box = ScrolledText(frame, height=7)
        self.text_box.pack(fill=tk.X, expand=False)

    def create_weapon_buttons(self, controller) -> None:
        """
        Creates weapon option buttons for the game.

        Parameters
        ----------
        controller :
            Controller instance.
        """
        frame = tk.Frame(self.window)
        frame.pack(fill=tk.BOTH, expand=False)

        # A loop does not work because the buttons must be created with their own variable.
        button_rock = tk.Button(frame, text=Weapons.ROCK.name.title(),
                                command=lambda: controller.play(Weapons.ROCK.value))
        button_rock.pack(side=tk.LEFT, padx=5)

        button_paper = tk.Button(frame, text=Weapons.PAPER.name.title(),
                                 command=lambda: controller.play(Weapons.PAPER.value))
        button_paper.pack(side=tk.LEFT, padx=5)

        button_scissors = tk.Button(frame, text=Weapons.SCISSORS.name.title(),
                                    command=lambda: controller.play(Weapons.SCISSORS.value))
        button_scissors.pack(side=tk.LEFT, padx=5)

        button_lizard = tk.Button(frame, text=Weapons.LIZARD.name.title(),
                                  command=lambda: controller.play(Weapons.LIZARD.value))
        button_lizard.pack(side=tk.LEFT, padx=5)

        button_spock = tk.Button(frame, text=Weapons.SPOCK.name.title(),
                                 command=lambda: controller.play(Weapons.SPOCK.value))
        button_spock.pack(side=tk.LEFT, padx=5)

    def info(self, text: str) -> None:
        showinfo(title='Information', message=text)

    def play(self, text: str) -> None:
        self.text_box.insert(tk.END, text)
        self.text_box.see(tk.END)

    def setup(self, controller) -> None:
        self.create_menu(controller)
        self.create_text_area()
        self.create_weapon_buttons(controller)

    def run(self) -> None:
        self.window.mainloop()
