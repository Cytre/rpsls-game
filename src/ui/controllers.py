"""
Library containing all controller related classes.

Author:
    Kevin D. Reeves <kevin@kevindreeves.com>
Copyright:
    2021 Kevin D. Reeves
Module:
    src.ui.controllers
Package:
    src.ui
"""

from abc import ABC, abstractmethod
from src.game import Game
from src.ui.views import AbstractView


class AbstractController(ABC):
    """Controller base class abstraction."""

    @abstractmethod
    def help(self) -> None:
        """Displays the help information."""

    @abstractmethod
    def play(self, choice: int) -> None:
        """
        Plays a round of the game.

        Parameters
        ----------
        choice : int
            The player's selected weapon choice.
        """

    @abstractmethod
    def rules(self) -> None:
        """Displays the rules information."""

    @abstractmethod
    def run(self) -> None:
        """Runs the game application."""


class GameController(AbstractController):
    """Game controller to run and play the game."""

    def __init__(self, game: Game, view: AbstractView) -> None:
        """
        Initializes the game and view instances.

        Parameters
        ----------
        game : Game
            The game class instance.
        view : AbstractView
            The view class instance.
        """
        self.game = game
        self.view = view

    def help(self) -> None:
        self.view.info(self.game.get_info('help'))

    def play(self, choice: int) -> None:
        self.view.play(self.game.play(choice))

    def rules(self) -> None:
        self.view.info(self.game.get_info('rules'))

    def run(self) -> None:
        self.view.setup(self)
        self.view.run()
