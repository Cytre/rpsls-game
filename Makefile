.PHONY: lint
.IGNORE: lint

lint:
	pylint --rcfile=.pylintrc ./src
