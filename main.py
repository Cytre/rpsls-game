#!/usr/bin/env python3

"""
Rock Paper Scissors Lizard Spock

Author:
    Kevin D. Reeves <kevin@kevindreeves.com>
Copyright:
    2015 Kevin D. Reeves
Since:
    2015-06-01
"""
import argparse

from src.game import Game
from src.ui.controllers import GameController
from src.ui.views import TkView
from src.utils.exceptions import WeaponsError


def main_terminal():
    """Run the terminal version of the game"""
    run = True
    game = Game()
    print(game.get_info('help'))

    while run:
        player_choice = input("What is your choice? ").lower()

        if player_choice == 'help':
            print(game.get_info('help'))
            continue
        elif player_choice == 'rules':
            print(game.get_info('rules'))
            continue
        else:
            try:
                print(game.play(player_choice))
            except WeaponsError as e:
                print(e)
                continue

        end = input("Play another round? Type yes or no: ").lower()

        if end in ('no', 'n'):
            run = False

    print('Thank you for playing!')


def main_gui():
    """Runs the GUI version of the game"""
    controller = GameController(Game(), TkView())
    controller.run()


def main():
    """Run the game"""
    parser = argparse.ArgumentParser(description='Rock Paper Scissors Lizard Spock Game')
    parser.add_argument('-gui', help='Run game in GUI mode', action='store_true')
    args = parser.parse_args()

    if args.gui:
        main_gui()
    else:
        main_terminal()


if __name__ == '__main__':
    main()
